@lazyGlobal off.

local targetApoapsis to 100000.

lock throttle to 1.0.
print "Blast off!".
stage.

until ship:apoapsis > targetApoapsis {
    // Heading at 0 should be 90, at 70km should be 0
    // Primitive calculation would just be max(90 - ((ship:altitude / kerbin:atm:height) * 90), 0)
    local targetHeading to max(90 - ((ship:altitude / kerbin:atm:height) * 90), 0).
    lock steering to heading(90, targetHeading).
}

print "100km apoapsis reached".

lock throttle to 0.
lock steering to heading(90, 0).

// Check if we're still in atm
wait until ship:altitude > kerbin:atm:height.
ag1 on.
if ship:apoapsis < targetApoapsis {
    print "Correcting for atm".
    until ship:apoapsis >= targetApoapsis {
        local diff to targetApoapsis - ship:apoapsis.
        lock throttle to min(diff / 10, 1).
    }
    lock throttle to 0.
    print "100km apoapsis reached".
}

lock throttle to 0.

print "Creating circularization node".
local circNode to node(0, 0, 0, 0).
set circNode:eta to ship:orbit:eta:apoapsis.
add circNode.

until abs(ship:orbit:apoapsis - circNode:orbit:periapsis) < 10 {
    local dvChange to 1.
    if abs(ship:orbit:apoapsis - circNode:orbit:periapsis) < 1000 {
        set dvChange to 0.1.
    }
    if circNode:orbit:periapsis < ship:orbit:apoapsis {
        set circNode:prograde to circNode:prograde + dvChange.
    } else {
        set circNode:prograde to circNode:prograde - dvChange.
    }
}

run execute_node.

unlock throttle.
unlock steering.

print "Ascent complete!".
