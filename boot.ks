@lazyGlobal off.
wait until ship:unpacked.
set ship:control:pilotmainthrottle to 0.
clearscreen.
core:part:getmodule("kOSProcessor"):doevent("Open Terminal").
set terminal:charheight to 18.

// Copy files over to the ship
print "Copying files to ship".
copyPath("0:/kos-scripts/kos-scripts/", "").
cd("kos-scripts").

print "Boot complete".